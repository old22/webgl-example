import * as wasm from "./pkg/webgl";
const webClient = new wasm.WebClient();
webClient.start();
// webClient.render();

let time = Date.now();
function render () {
    const dt = Date.now() - time;

    webClient.update(dt);
    webClient.render();
    window.requestAnimationFrame(render);

    time = Date.now()
}

render();