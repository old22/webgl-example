use wasm_bindgen::prelude::*;
use web_sys::{WebGlRenderingContext};
mod canvas;
mod controls;
mod shader;
mod render;
mod load_texture_image;
mod app;

use canvas::{create_webgl_context, get_canvas};
use controls::append_controls;
use render::WebRenderer;
use load_texture_image::load_texture_image;
use app::{App, Msg};
use std::rc::Rc;


/// Used to run the application from the web
#[wasm_bindgen]
pub struct WebClient {
    app: Rc<App>,
    gl: Rc<WebGlRenderingContext>,
    renderer: WebRenderer,
}

#[wasm_bindgen]
impl WebClient {
    /// Constructor method, create a new web client.
    #[wasm_bindgen(constructor)]
    pub fn new() -> WebClient {
        console_error_panic_hook::set_once();

        let app = Rc::new(App::new());

        // let document = web_sys::window().unwrap().document().unwrap();
        // let cv = get_canvas(Rc::clone(&app)).unwrap();

        let gl = Rc::new(
            create_webgl_context(
                Rc::clone(&app)
            ).unwrap()
        );

        append_controls(Rc::clone(&app)).expect("Append controls");

        let renderer = WebRenderer::new(&gl);

        WebClient { app, gl, renderer }
    }

    /// Start the WebGL rendering application. `index.html` will call this function in order
    /// to begin rendering.
    pub fn start(&self) -> Result<(), JsValue> {
        let gl = &self.gl;

        // Load image texture. It will be saved on ``TEXTURE0'' buffer.
        load_texture_image(
            Rc::clone(gl),
            "/images/earthmap1k.jpg",
        );

        // That's all, return OK.
        Ok(())
    }

    /// Render the scene. `index.html`.
    pub fn render(&mut self) {
        self.renderer.render(&self.gl, &self.app.store.borrow().state);
    }

    /// Update simulation time.
    pub fn update(&self, dt: f32) {
        self.app.store.borrow_mut().msg(&Msg::AdvanceClock(dt));
    }
}
