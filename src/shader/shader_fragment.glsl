/*
precision lowp float;
varying vec4 fragment_color;
void main() {
    gl_FragColor = fragment_color;
}
*/

/*
precision lowp float;
varying highp vec2 vTextureCoord;
uniform sampler2D uSampler;
// uniform vec4 color = vec4(1.0, 0.0, 0.0, 1.0);
void main() {
    //gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    gl_FragColor = texture2D(uSampler, vTextureCoord);
}
*/

precision mediump float;

varying vec2 texCoords;

uniform sampler2D texture;

void main() {
    //gl_FragColor = texture2D( texture, vec2(texCoords.s, texCoords.t) );
    gl_FragColor = texture2D( texture, texCoords );
}
