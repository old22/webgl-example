//! # Shader
//!
//! Manages the WebGl shaders. Two GLSL shaders are implemented:
//! * `shader_vertex.glsl`: Vertices positions.
//! * `shader_fragment.glsl`: Colors and textues.
//!
//! The mmanagement is carried by the `shaders.rs` module.

mod shaders;
pub use shaders::Shader;

pub static VERTEX_SHADER: &'static str = include_str!("shader_vertex.glsl");
pub static FRAGMENT_SHADER: &'static str = include_str!("shader_fragment.glsl");

/*
pub fn get_vertex_shader(
    context: &WebGlRenderingContext) -> Result<WebGlShader, String>
{
    compile_shader(
        &context,
        WebGlRenderingContext::VERTEX_SHADER,
        VERTEX_SHADER)
}

pub fn get_fragment_shader(
    context: &WebGlRenderingContext) -> Result<WebGlShader, String>
{
    compile_shader(
        &context,
        WebGlRenderingContext::FRAGMENT_SHADER,
        FRAGMENT_SHADER)
}
*/