/*
attribute vec4 position;
attribute vec4 vertex_color;
varying vec4 fragment_color;
void main() {
    gl_Position = position;
    fragment_color = vertex_color;
}
*/

/*
attribute vec4 position;
attribute vec2 aTextureCoord;

//uniform mat4 uModelViewMatrix;
//uniform mat4 uProjectionMatrix;

varying highp vec2 vTextureCoord;
//uniform sampler2D uSampler;

void main(void) {
    //gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    gl_Position = position;
    vTextureCoord = aTextureCoord;
}
*/

vec4 quat_mult(vec4 q1, vec4 q2){
    float q3_q4 = q1.w * q2.w - dot(q1.xyz, q2.xyz);
    vec3 q3_v = q1.w * q2.xyz + q2.w * q1.xyz + cross(q1.xyz, q2.xyz);
    return vec4(q3_v.xyz, q3_q4);
}

vec4 conj(vec4 q){
    return vec4(-q.xyz, q.w);
}

attribute vec3 position;

varying vec2 texCoords;
attribute vec2 aTextureCoord;
uniform vec4 rotation;

void main() {
    gl_Position = vec4(quat_mult(rotation, quat_mult(vec4(position.xyz, 0.0), conj(rotation))).xyz, 1.0);
    texCoords = aTextureCoord;
}
