use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{WebGlRenderingContext, HtmlImageElement};
use web_sys::WebGlRenderingContext as GL;
use web_sys::console;
use js_sys;
use std::rc::Rc;
use std::cell::RefCell;

/// Load a texture on *gl* WebGl by isng the *src* URL. Texture will be loaded on TEXTURE0
/// asynchronously.
pub fn load_texture_image(
    gl: Rc<WebGlRenderingContext>,
    src: &str)
{
    let image = Rc::new(RefCell::new(HtmlImageElement::new().unwrap()));
    let image_clone = Rc::clone(&image);

    let onload = Closure::wrap(Box::new(move || {
        let texture = gl.create_texture();

        gl.active_texture(GL::TEXTURE0);

        gl.bind_texture(GL::TEXTURE_2D, texture.as_ref());

        gl.pixel_storei(GL::UNPACK_FLIP_Y_WEBGL, 1);

        if is_power_of_2(image_clone.borrow().width()) && is_power_of_2(image_clone.borrow().height()) {
            // Yes, it's a power of 2. Generate mips.
            gl.generate_mipmap(GL::TEXTURE_2D);
        } else {
            // No, it's not a power of 2. Turn of mips and set
            // wrapping to clamp to edge
            gl.tex_parameteri(GL::TEXTURE_2D, GL::TEXTURE_WRAP_S, GL::CLAMP_TO_EDGE as i32);
            gl.tex_parameteri(GL::TEXTURE_2D, GL::TEXTURE_WRAP_T, GL::CLAMP_TO_EDGE as i32);
            gl.tex_parameteri(GL::TEXTURE_2D, GL::TEXTURE_MIN_FILTER, GL::LINEAR as i32);
        }

        gl.tex_image_2d_with_u32_and_u32_and_image(
            GL::TEXTURE_2D,
            0,
            GL::RGBA as i32,
            GL::RGBA,
            GL::UNSIGNED_BYTE,
            &image_clone.borrow(),
        )
            .expect("Texture image 2d");
        let array = js_sys::Array::new();
        array.push(&"Hi! code did not panic, texture load OK.".into());
        console::log(&array);
    }) as Box<dyn Fn()>);

    let image = image.borrow_mut();

    image.set_onload(Some(onload.as_ref().unchecked_ref()));
    image.set_src(src);

    onload.forget();
}

/// Check if value is power of 2.
/// A binary representation of a number which is power of 2 is all zeros except one figure:
///
/// 00010000
///
/// Take one and said figure turns zero and the previous figures into one:
///
/// 00001111
///
/// Thus, an AND operation must return zero:
/// ```
///   00010000
/// & 00001111
///   --------
///   00000000
/// ```
fn is_power_of_2(value: u32) -> bool {
    return (value & (value - 1)) == 0;
}