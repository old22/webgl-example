use wasm_bindgen::prelude::*;
use js_sys::WebAssembly;
use web_sys::console;
use web_sys::WebGlRenderingContext;
use web_sys::WebGlRenderingContext as GL;
use web_sys::WebGlProgram;
use wasm_bindgen::JsCast;
use crate::shader::{Shader,VERTEX_SHADER, FRAGMENT_SHADER};
use crate::canvas::{CANVAS_HEIGHT, CANVAS_WIDTH};
use crate::app::State;
use std::f32::consts::PI;

// const ROTATION_AXIS: [f32; 3] = [0.57735026918962576451; 3];
const ROTATION_AXIS: [f32; 3] = [0.0, -1.0, 0.0];
const SCALE: f32 = 0.6;

const N_LAMBDA: usize = 18;
const N_PHI: usize = 20;
const R: f32 = 1.0;

const N_POINTS: usize = N_LAMBDA * N_PHI * 3;
const N_COORDS: usize = N_LAMBDA * N_PHI * 2;
const N_IDX: usize = 6 * (N_LAMBDA - 1) * (N_PHI - 1);

pub struct WebRenderer {
    shader: Shader,
    #[allow(unused)]
    depth_texture_ext: Option<js_sys::Object>
}

impl WebRenderer {
    pub fn new(gl: &WebGlRenderingContext) -> WebRenderer {
        let shader = Shader::new(&gl, VERTEX_SHADER, FRAGMENT_SHADER).unwrap();

        let depth_texture_ext = gl
            .get_extension("WEBGL_depth_texture")
            .expect("Depth texture extension");

        WebRenderer {
            shader,
            depth_texture_ext,
        }
    }

    pub fn render(&mut self, gl: &WebGlRenderingContext, state: &State) {

        gl.clear_color(0.53, 0.8, 0.98, 1.);
        gl.clear(GL::COLOR_BUFFER_BIT | GL::DEPTH_BUFFER_BIT);

        gl.clear_color(0.0, 0.0, 0.0, 1.);
        gl.clear(GL::COLOR_BUFFER_BIT | GL::DEPTH_BUFFER_BIT);

        let above = 1000000.0;
        // Position is positive instead of negative for.. mathematical reasons..
        let _clip_plane = [0., 1., 0., above];

        gl.viewport(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

        // gl.bind_framebuffer(GL::FRAMEBUFFER, None);

        let s: usize = self.init_buffers_sphere(gl);
        self.set_rotation(gl, &state);
        self.render_background(gl, s as i32).unwrap();
    }

    fn set_rotation(&mut self, gl: &GL, state: &State) {
        let time: f32 = state.clock() % (state.period() *1e3);
        // let time = 2500.0;
        let phi: f32 = 2e0 * PI * time * 1e-3 / state.period();
        let mut q: [f32; 4] = [0.0; 4];
        for i in 0..3 { q[i] = ROTATION_AXIS[i] * (phi / 2e0).sin(); }
        q[3] = (phi / 2e0).cos();

        let shader = &self.shader;
        gl.use_program(Some(&shader.program));
        let uni_rotation = shader.get_uniform_location(gl, "rotation");

        gl.uniform4fv_with_f32_array(uni_rotation.as_ref(), &mut q);

        let array = js_sys::Array::new();
        let content = format!("dt: {}", time);
        array.push(&content.into());
        // console::log(&array);
    }

    fn init_buffers_sphere(&mut self, context: &GL) -> usize
    {
        let mut vertices: [f32; N_POINTS] = [0.0; N_POINTS];
        let mut coords: [f32; N_COORDS] = [0.0; N_COORDS];
        let mut index: [u16; N_IDX] = [0; N_IDX];

        let mut count: usize = 0;

        for i_phi in 0..(N_PHI) {

            let tex_phi: f32 = (i_phi as f32) / (N_PHI as f32 - 1e0);
            let phi: f32 = 2e0 * (tex_phi - 5e-1) * PI / 2e0;

            for i_lambda in 0..(N_LAMBDA) {

                let tex_lambda: f32 = (i_lambda as f32) / (N_LAMBDA as f32 - 1e0);
                let lambda: f32 = 2e0 * (tex_lambda - 5e-1) * PI;

                let vertex: [f32; 3] = [

                    R * phi.cos() * lambda.cos(),
                    R * phi.sin(),
                    R * phi.cos() * lambda.sin(),

                ];

                let position: [f32; 2] = [tex_lambda, tex_phi];

                vertices[3 * count + 0] = vertex[0] * SCALE;
                vertices[3 * count + 1] = vertex[1] * SCALE;
                vertices[3 * count + 2] = vertex[2] * SCALE;


                coords[2 * count + 0] = position[0];
                coords[2 * count + 1] = position[1];

                count = count + 1;
            }
        }

        count = 0;

        for i_phi in 0..(N_PHI - 1) {
            for i_lambda in 0..(N_LAMBDA - 1) {
                let ref_0: u16 =  (i_phi * N_LAMBDA + i_lambda) as u16;
                let ref_1: u16 = ref_0 + 1;
                let ref_2: u16 = ref_0 + N_LAMBDA as u16;
                let ref_3: u16 = ref_2 + 1;

                index[count * 6 + 0] = ref_0;
                index[count * 6 + 1] = ref_3;
                index[count * 6 + 2] = ref_2;

                index[count * 6 + 3] = ref_0;
                index[count * 6 + 4] = ref_1;
                index[count * 6 + 5] = ref_3;

                count = count + 1;
            }
        }

        // Get Vertex shader attribute identifiers.
        let attrib_vertex_position = context.get_attrib_location(&self.shader.program, "position");
        let attrib_vertex_position_32 = attrib_vertex_position as u32;

        // Create and enable vertices' position attribute buffer.
        buffer_f32_data(&context,&vertices, attrib_vertex_position_32, 3);
        context.enable_vertex_attrib_array(attrib_vertex_position_32);


        let attrib_texture_coord = context.get_attrib_location(&self.shader.program, "aTextureCoord");
        let attrib_texture_coord_32 = attrib_texture_coord as u32;
        // Create and enable vertices' position attribute buffer.
        buffer_f32_data(&context,&coords, attrib_texture_coord_32, 2);
        context.enable_vertex_attrib_array(attrib_texture_coord_32);

        // Create vertices' indices buffer.
        buffer_u16_indices(&context, &mut index);

        return index.len();
    }

    fn init_buffers_background(&mut self, context: &GL)
    {
        //let vertex_data = self.make_textured_quad_vertices(CANVAS_WIDTH, CANVAS_HEIGHT);

        // Texture coordinates.
        let texture_coordinates_face: [f32; 8] = [
            // Front
            0.0,  0.0,
            1.0,  0.0,
            1.0,  1.0,
            0.0,  1.0
        ];

        let mut texture_coordinates: [f32; 48] = [0.0 as f32; 48];
        for i in 0..6 {
            for j in 0..8 {
                texture_coordinates[i*8 + j] = texture_coordinates_face[j];
            }

        }

        // Triangle vertices.
        /*let vertices: [f32; 8] = [
            -0.8, -0.8, // Bottom Left
            0.8, -0.8, // Bottom Right
            0.8, 0.8, // Top Right
            -0.8, 0.8, // Top Left
        ];*/

        let mut vertices: [f32; 72] = [
            // Front face
            -1.0, -1.0,  1.0,
            1.0, -1.0,  1.0,
            1.0,  1.0,  1.0,
            -1.0,  1.0,  1.0,

            // Back face
            -1.0, -1.0, -1.0,
            -1.0,  1.0, -1.0,
            1.0,  1.0, -1.0,
            1.0, -1.0, -1.0,

            // Top face
            -1.0,  1.0, -1.0,
            -1.0,  1.0,  1.0,
            1.0,  1.0,  1.0,
            1.0,  1.0, -1.0,

            // Bottom face
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0, -1.0,  1.0,
            -1.0, -1.0,  1.0,

            // Right face
            1.0, -1.0, -1.0,
            1.0,  1.0, -1.0,
            1.0,  1.0,  1.0,
            1.0, -1.0,  1.0,

            // Left face
            -1.0, -1.0, -1.0,
            -1.0, -1.0,  1.0,
            -1.0,  1.0,  1.0,
            -1.0,  1.0, -1.0
        ];

        for i in 0..vertices.len() {
            vertices[i] = vertices[i] * SCALE;
        }

        // Triangle vertices' indices.
        //let mut indices: [u16; 6] = [0, 1, 2, 0, 2, 3];
        let mut indices: [u16; 36] = [
            0,  1,  2,      0,  2,  3,    // front
            4,  5,  6,      4,  6,  7,    // back
            8,  9,  10,     8,  10, 11,   // top
            12, 13, 14,     12, 14, 15,   // bottom
            16, 17, 18,     16, 18, 19,   // right
            20, 21, 22,     20, 22, 23,   // left
        ];

        // Get Vertex shader attribute identifiers.
        let attrib_vertex_position = context.get_attrib_location(&self.shader.program, "position");
        let attrib_vertex_position_32 = attrib_vertex_position as u32;

        // Create and enable vertices' position attribute buffer.
        buffer_f32_data(&context,&vertices, attrib_vertex_position_32, 3);
        context.enable_vertex_attrib_array(attrib_vertex_position_32);


        let attrib_texture_coord = context.get_attrib_location(&self.shader.program, "aTextureCoord");
        let attrib_texture_coord_32 = attrib_texture_coord as u32;
        // Create and enable vertices' position attribute buffer.
        buffer_f32_data(&context,&texture_coordinates, attrib_texture_coord_32, 2);
        context.enable_vertex_attrib_array(attrib_texture_coord_32);

        // Create vertices' indices buffer.
        buffer_u16_indices(&context, &mut indices);
    }

    fn render_background(&mut self, context: &GL, s: i32) -> Result<(), JsValue>{
        let shader = &self.shader;
        context.use_program(Some(&shader.program));

        let uni_texture = shader.get_uniform_location(context, "texture");
        context.uniform1i(uni_texture.as_ref(), 0);

        context.enable(GL::BLEND);
        context.blend_func(GL::SRC_ALPHA, GL::ONE_MINUS_SRC_ALPHA);
        // context.draw_elements_with_i32(GL::TRIANGLES, s, GL::UNSIGNED_SHORT, 0);
        context.draw_elements_with_i32(GL::TRIANGLES, s, GL::UNSIGNED_SHORT, 0);
        // context.draw_arrays(GL::TRIANGLES, 0, s);
        context.disable(GL::BLEND);

        // Return (OK).
        Ok(())
    }

}

/// Allocate WebAssembly memory for data buffering, create WebGl buffer on *gl*
/// WebGlRenderingContext instance, pass f32 array *data* which goes on packs of *size* number of
/// elements depending on its applications.
fn buffer_f32_data(gl: &GL, data: &[f32], attrib: u32, size: i32) {
    let memory_buffer = wasm_bindgen::memory()
        .dyn_into::<WebAssembly::Memory>()
        .unwrap()
        .buffer();

    let data_location = data.as_ptr() as u32 / 4;
    let data_array = js_sys::Float32Array::new(&memory_buffer)
        .subarray(data_location, data_location + data.len() as u32);
    let buffer = gl.create_buffer().unwrap();

    gl.bind_buffer(GL::ARRAY_BUFFER, Some(&buffer));
    gl.buffer_data_with_array_buffer_view(GL::ARRAY_BUFFER, &data_array, GL::STATIC_DRAW);
    gl.vertex_attrib_pointer_with_i32(attrib, size, GL::FLOAT, false, 0, 0);
}

/// Allocate WebAssembly memory for data buffering, create WebGl buffer on *gl*
/// WebGlRenderingContext instance, pass u16 array *data* which goes on packs of *size* number of
/// elements depending on its applications.
fn buffer_u16_indices(gl: &GL, indices: &[u16]) {
    let memory_buffer = wasm_bindgen::memory()
        .dyn_into::<WebAssembly::Memory>()
        .unwrap()
        .buffer();

    let indices_location = indices.as_ptr() as u32 / 2;
    let indices_array = js_sys::Uint16Array::new(&memory_buffer)
        .subarray(indices_location, indices_location + indices.len() as u32);
    let buffer = gl.create_buffer().unwrap();

    gl.bind_buffer(GL::ELEMENT_ARRAY_BUFFER, Some(&buffer));
    gl.buffer_data_with_array_buffer_view(GL::ELEMENT_ARRAY_BUFFER, &indices_array, GL::STATIC_DRAW);
    //gl.vertex_attrib_pointer_with_i32(attrib, size, GL::FLOAT, false, 0, 0);
}