# Rust + Wasm + WebGl Example

Example of WebGl program written on Rust by using Wasm, based on
[this GitHub project](https://github.com/chinedufn/webgl-water-tutorial).

The final aim is to display the attitude of an spacecraft as a 3D animation.